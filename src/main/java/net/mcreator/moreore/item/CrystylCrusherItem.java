
package net.mcreator.moreore.item;

@MoreOreModElements.ModElement.Tag
public class CrystylCrusherItem extends MoreOreModElements.ModElement {

	@ObjectHolder("more_ore:crystyl_crusher")
	public static final Item block = null;

	public CrystylCrusherItem(MoreOreModElements instance) {
		super(instance, 105);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new PickaxeItem(new IItemTier() {
			public int getMaxUses() {
				return 100000;
			}

			public float getEfficiency() {
				return 40000f;
			}

			public float getAttackDamage() {
				return 12981f;
			}

			public int getHarvestLevel() {
				return 10000;
			}

			public int getEnchantability() {
				return 2000;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.EMPTY;
			}
		}, 1, 96f, new Item.Properties().group(MoreOreItemGroup.tab).isImmuneToFire()) {

			@Override
			public void addInformation(ItemStack itemstack, World world, List<ITextComponent> list, ITooltipFlag flag) {
				super.addInformation(itemstack, world, list, flag);
				list.add(new StringTextComponent("Giganigga"));
			}

			@Override
			@OnlyIn(Dist.CLIENT)
			public boolean hasEffect(ItemStack itemstack) {
				return true;
			}

		}.setRegistryName("crystyl_crusher"));
	}

}
