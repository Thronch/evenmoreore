
package net.mcreator.moreore.item;

@MoreOreModElements.ModElement.Tag
public class GildedBucketItem extends MoreOreModElements.ModElement {

	@ObjectHolder("more_ore:gilded_bucket")
	public static final Item block = null;

	public GildedBucketItem(MoreOreModElements instance) {
		super(instance, 106);

	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemCustom());
	}

	public static class ItemCustom extends Item {

		public ItemCustom() {
			super(new Item.Properties().group(MoreOreItemGroup.tab).maxStackSize(64).isImmuneToFire().rarity(Rarity.EPIC));
			setRegistryName("gilded_bucket");
		}

		@Override
		public int getItemEnchantability() {
			return 0;
		}

		@Override
		public int getUseDuration(ItemStack itemstack) {
			return 0;
		}

		@Override
		public float getDestroySpeed(ItemStack par1ItemStack, BlockState par2Block) {
			return 1F;
		}

	}

}
